<?php

/**
 * @file
 *   Drush command to build themes
 */

/**
 * Implementation of hook_drush_command().
 */
function drush_theme_builder_drush_command() {
  $items = array();

  $items['theme-build'] = array(
    'callback' => 'drush_theme_builder_theme_build',
    'description' => "Builds a theme",
    'arguments' => array(
      'theme_name' => 'Machine-readable theme name',
    ),
    'options' => array(
      'name' => 'Human-readable theme name',
      'description' => 'Theme description',
      'base-theme' => 'Base theme name',
      'files' => 'Comma-separated list of files to build, "style", "script" and "template" are supported. Info file is always built',
    ),
    'examples' => array(
      'drush tb test --name="Test theme" --description="Simple test theme" --files=style,script' => 'Build theme called test with style.css and script.js'
    ),
    'aliases' => array('tb'),
  );

  return $items;
}

/**
 * drush command callback. This is where the action takes place.
 *
 */
 
function drush_theme_builder_theme_build($theme_name = NULL) {

  if ($theme_name) {
  
    $files = explode(',', drush_get_option('files'));
    
    $build_files[$theme_name . '.info'] = _drush_theme_builder_build_info($theme_name, drush_get_option('name'), drush_get_option('description'), drush_get_option('base-theme'), $files);
    
    if (in_array('style', $files)) {
      $build_files['style.css'] = _drush_theme_builder_build_css();
    }
    if (in_array('script', $files)) {
      $build_files['script.js'] = _drush_theme_builder_build_js();
    }
    if (in_array('template', $files)) {
      $build_files['template.php'] = _drush_theme_builder_build_template();
    }
 
    foreach ($build_files as $build_file_name => $build_file_content) {
      _drush_theme_builder_output_file($theme_name, $build_file_name, $build_file_content);
    } 
  
  } else {

    drush_log(dt('You must specify a theme name. Use --help option to get the full syntax'), 'error');
    
  }

}


function _drush_theme_builder_build_info($theme_name, $theme_fullname = NULL, $description = NULL, $base_theme = NULL, $files = array()) {
  
    $info = array(
      'name' => $theme_fullname ? $theme_fullname : $theme_name,
      'description' => $description,
      'base theme' => $base_theme,
      'core' => drush_drupal_major_version() . '.x',
      'engine' => 'phptemplate',
      'stylesheets[all][]' => in_array('style', $files) ? 'style.css' : NULL,
      'scripts[]' => in_array('script', $files) ? 'script.js' : NULL,
    );
  
  foreach ($info as $key => $value) {
      if (is_numeric($key)) {
        $output .= $value . "\n";
      } else {
        if ($value) {
          $output .= $key . ' = ' . $value . "\n";
        }  
      }
    }
  return $output;
}


function _drush_theme_builder_build_css() {

  return $output;

}


function _drush_theme_builder_build_js() {
  
  return $output;

}

function _drush_theme_builder_build_template() {
 
  $output = '<?php
  
';
  return $output;

}

function _drush_theme_builder_output_file($theme_name, $file_name, $file_contents) {

  // Fix from http://drupal.org/node/1194556#comment-5169408
  
  require_once DRUSH_BASE_PATH . '/commands/pm/download.pm.inc';  
  $theme_dir = _pm_download_destination('theme') . '/' .$theme_name;
  
  $file_path = $theme_dir . '/' . $file_name;

  
    if (!is_dir(_pm_download_destination('theme'))) { 
      @drush_op('mkdir', _pm_download_destination('theme'));      
    }
    if (!is_dir($theme_dir)) {
      @drush_op('mkdir', $theme_dir);
    }
  
    file_put_contents($file_path, $file_contents);

    drush_log(dt('!file-name was written to !path', array('!file-name' => $file_name, '!path' => $theme_dir)), 'success');

}